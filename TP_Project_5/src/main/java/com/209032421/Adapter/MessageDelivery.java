package com.209032421.Adapter;

public class MessageDelivery {

    private String message;
    private String sender;

    public MessageDelivery(){}

    public MessageDelivery(String message, String sender){
        setMessage(message);
        setSender(sender);
    }
    public String getMessage() {
        return message;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}