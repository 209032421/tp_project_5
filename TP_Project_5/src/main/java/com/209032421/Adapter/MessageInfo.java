package com.209032421.Adapter;

public interface MessageInfo {

    public String getMessage();
    public String getSender();

    public void setMessage(String msg);
    public void setSender(String sender);

}
