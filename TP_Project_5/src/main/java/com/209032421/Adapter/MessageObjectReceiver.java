package com.209032421.Adapter;

public class MessageObjectReceiver implements MessageInfo{
//This class implements Composition.

    MessageDelivery messageDelivery;

    public MessageObjectReceiver(){
        messageDelivery = new MessageDelivery();
    }

    public void setMessage(String msg) {
        messageDelivery.setMessage(msg);
    }

    public void setSender(String sender) {
        messageDelivery.setSender(sender);
    }

    public String getSender() {
        return messageDelivery.getSender();
    }

    public String getMessage() {
        return messageDelivery.getMessage();
    }
}