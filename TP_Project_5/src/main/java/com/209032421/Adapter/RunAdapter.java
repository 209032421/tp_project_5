package com.209032421.Adapter;

public class DemoAdapter {

    public static void main(String[] args)
    {
        //Example of Class Adapter, implements inheritance
        System.out.println("Class Adapter Test");
        MessageInfo classMessageInfo = new MessageClassReceiver();
        classMessageInfo.setMessage("We are so perfect we finish each others..");
        classMessageInfo.setSender(" GIRL");
        displayMessage(classMessageInfo);


        //Example of Object Adapter, implements composition
        System.out.println("\nObject Adapter Test");
        MessageInfo objectMessageInfo = new MessageObjectReceiver();
        objectMessageInfo.setMessage("Food??");
        objectMessageInfo.setSender("BOY");
        displayMessage(objectMessageInfo);
    }

    public static void displayMessage(MessageInfo info){

        System.out.println(info.getSender().toUpperCase()+":"+" Message: " + info.getMessage());
        System.out.println();

    }
}
