/*
Chapter 5 - Design Patterns
    Demonstrate the following Design Patterns WITH test cases

    1. Singleton
    2. Abstract Factory
    3. Prototype
    4. Adapter
    5. Bridge
    6. Proxy
    7. Chain of Responsibility
    8. Visitor
    9. Strategy

 */

package com.209032421;

public class App
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!,\n Each Package has it's own runnable demo" );
    }
}
