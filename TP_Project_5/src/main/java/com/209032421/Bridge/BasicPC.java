package com.209032421.Bridge;

public class BasicPC extends Computer {

    public BasicPC(Processor processor){
        this.power = 25;
        this.processor = processor;
    }
    @Override
    public void startPC() {
        System.out.println("Entry Level PC");
        System.out.println("Pc is booting up...");
        int processorSpeed = processor.run();
        powerRatings(processorSpeed);

    }
}