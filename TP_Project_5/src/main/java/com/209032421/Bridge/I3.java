package com.209032421.Bridge;

public class I3 implements Processor{
    int powerRating;

    public I3(){
        powerRating = 10;
    }
    public int run() {
        System.out.println("Entry level processor");
        System.out.println("i3 Processor: 750 MHZ");
        return powerRating;
    }
}