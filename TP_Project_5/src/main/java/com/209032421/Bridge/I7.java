package com.209032421.Bridge;

public class I7 implements Processor{
    int psu;

    public I7(){
        psu = 20;
    }

    public int run() {
        System.out.println("High end processor");
        System.out.println("i7 Processor: 3.0GHZ.");
        return psu;
    }
}