package com.209032421.Bridge;

public class PerformancePC extends Computer{

    public PerformancePC(Processor processor){
        this.power = 100;
        this.processor = processor;
    }
    @Override
    public void startPC() {
        System.out.println("High End PC");
        System.out.println("Pc is booting up...");
        int processorSpeed = processor.run();
        powerRatings(processorSpeed);

    }
}
