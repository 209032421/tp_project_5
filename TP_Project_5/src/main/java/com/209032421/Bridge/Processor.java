package com.209032421.Bridge;

public interface Processor {
    public int run();
}