package com.209032421.Bridge;

public class RunBridge {

    public static void main(String[] args){

        Computer junk = new BasicPC(new I3());

        Computer notJunk = new PerformancePC(new I7());


        junk.startPC();
        notJunk.startPC();
    }
}

