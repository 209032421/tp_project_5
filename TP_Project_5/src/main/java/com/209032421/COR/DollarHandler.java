package com.209032421.COR;

public class DollarHandler extends MoneyHandler {

    @Override
    public void handleRequest(CurrencyEnums request) {
        if (request == CurrencyEnums.Dollar)
        {
            System.out.println("DollarHandler handles " + request);
            System.out.println("U.S Currency\n");
        }
        else{
            System.out.println("DollarHandler doesn't handle " + request);
            if (successor != null){
                successor.handleRequest(request);
            }
        }
    }
}