package com.209032421.COR;

public class EuroHandler extends MoneyHandler{

    @Override
    public void handleRequest(CurrencyEnums request) {
        if (request == CurrencyEnums.Euro)
        {
            System.out.println("EuroHandler handles " + request);
            System.out.println("Official Currency of the Eurozone.\n");
        }
        else{
            System.out.println("EuroHandler doesn't handle " + request);
            if (successor != null){
                successor.handleRequest(request);
            }
        }
    }
}