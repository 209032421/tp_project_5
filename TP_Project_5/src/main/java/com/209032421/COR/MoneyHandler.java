package com.209032421.COR;

public abstract class MoneyHandler {
    MoneyHandler successor;

    public void setSuccessor(MoneyHandler successor){
        this.successor = successor;
    }

    public MoneyHandler getSuccessor() {
        return successor;
    }

    public abstract void handleRequest(CurrencyEnums request);
}