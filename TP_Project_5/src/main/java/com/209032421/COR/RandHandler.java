package com.209032421.COR;

public class RandHandler extends MoneyHandler {

    @Override
    public void handleRequest(CurrencyEnums request) {
        if (request == CurrencyEnums.Rand)
        {
            System.out.println("RandHandler handles " + request);
            System.out.println("South African Currency\n");
        }
        else{
            System.out.println("RandHandler doesn't handle " + request);
            if (successor != null){
                successor.handleRequest(request);
            }
        }
    }
}