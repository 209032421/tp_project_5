package com.209032421.COR;

public class RunChainOfResponsibility {

    public static void main(String[] args){
        MoneyHandler currencyChain = setUpChain();
        currencyChain.handleRequest(CurrencyEnums.Dollar);
        currencyChain.handleRequest(CurrencyEnums.Rand);
        currencyChain.handleRequest(CurrencyEnums.Euro);
    }

    public static MoneyHandler setUpChain(){
        MoneyHandler randHandler = new RandHandler();
        MoneyHandler dollarHandler = new DollarHandler();
        MoneyHandler euroHandler = new EuroHandler();

        randHandler.setSuccessor(dollarHandler);
        dollarHandler.setSuccessor(euroHandler);

        return randHandler;
    }
}