package com.209032421.Factory;

public class AbstractFactory{
    public CarFactory getCarFactory(String type){

        if("Basic".equals(type)){
            return new BasicFactory();
        }else if("Sport".equals(type)){
            return new SportFactory();
        }else{
            return new LuxuryFactory();
        }
    }

}