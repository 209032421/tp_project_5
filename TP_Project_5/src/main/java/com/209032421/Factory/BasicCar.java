package com.209032421.Factory;

public class BasicCar extends Car {
    @Override
    public void carDescription() {
        String description = "\nSimple basic car with poor speed and average comfort. \nSacrifices comfort and speed for lower price.";
        System.out.println(description);
    }

    @Override
    public int speed() {
        return 120;
    }
}