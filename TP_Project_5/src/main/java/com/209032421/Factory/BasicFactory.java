package com.209032421.Factory;

public class BasicFactory extends AbstractFactory{

    @Override
    public Car getCar(String type) {
        if("Basic".equals(type))
        {
            return new BasicCar();
        }
    }
}