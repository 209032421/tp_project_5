package com.209032421.Factory;

public abstract class Car {

    public abstract void carDescription();
    public abstract int speed();
}
