package com.209032421.Factory;

public abstract class CarFactory{
    public abstract Car getCar(String type);
}