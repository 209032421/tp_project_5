package com.209032421.Factory;

public class LuxuryCar extends Car {
    @Override
    public void carDescription() {
        String description = "\nMuch more comfortable and classier than a basic car. \nSacrifices speed and price for comfort.";
        System.out.println(description);
    }

    @Override
    public int speed() {
        return 190;
    }
}