package com.209032421.Factory;

public class LuxuryFactory extends AbstractFactory{

    @Override
    public Car getCar(String type) {
        if("Luxury".equals(type))
        {
            return new LuxuryCar();
        }else
        {
            return new BasicCar();
        }
    }
}