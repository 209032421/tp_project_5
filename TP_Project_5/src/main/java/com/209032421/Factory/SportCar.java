package com.209032421.Factory;

public class SportCar extends Car {
    @Override
    public void carDescription() {
        String description = "\nFaster and more powerfull than a basic car. \nSacrifices comfort for speed.";
        System.out.println(description);
    }

    @Override
    public int speed() {
        return 250;
    }
}