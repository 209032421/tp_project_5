package com.209032421.Factory;

public class SportFactory extends AbstractFactory{

    @Override
    public Car getCar(String type) {
        if("Sport".equals(type))
        {
            return new SportCar();
        }else
        {
            return new BasicCar();
        }
    }
}