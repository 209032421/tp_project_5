package com.209032421.Factory;

public class runAbstractFactory{

    public static void main(String[] args) {

        AbstractFactory abstractFactory = new AbstractFactory();

        CarFactory basicCars = abstractFactory.getCarFactory("Basic");
        CarFactory sportCars = abstractFactory.getCarFactory("Sport");
        CarFactory luxuryCars = abstractFactory.getCarFactory("Luxury");

        Car daewoo = basicCars.getCar("Basic");
        Car supra = sportCars.getCar("Sport");
        Car bentley = luxuryCars.getCar("Luxury");


        System.out.println("Basic Car Information: \n");
        daewoo.carDescription();
        System.out.println("Top Speed: " + daewoo.speed());

        System.out.println("Sport Car Information: \n");
        supra.carDescription();
        System.out.println("Top SPeed: " + supra.speed());

        System.out.println("Luxury Car Information: \n");
        bentley.carDescription();
        System.out.println("Top Speed: " + bentley.speed());


    }
}