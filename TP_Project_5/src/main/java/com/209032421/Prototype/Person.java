package com.209032421.Prototype;

public class Person implements Prototype{

    private String name;
    private int age;

    public Person(String name, int age){
        setName(name);
        setAge(age);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public Prototype makeDuplicate() {
        return new Person(name, age);
    }

    @Override
    public String toString() {
        return ("Personal Details: \nName: " + name + "\nAge:" + age);
    }
}