package com.209032421.Prototype;

public interface Prototype{

    public Prototype makeDuplicate();
}