package com.209032421.Prototype;

public class RunPrototype {

    public static void main(String[]args)
    {
        //Objects to clone
        Person child = new Person ("Jimmy", 14);
        Person adult = new Person ("Ellen", 24);


        //Clone objects
        Person anotherChild = (Person) child.createCopy();
        Person anotherAdult = (Person) adult.createCopy();


        System.out.println(child.toString());
        System.out.println(anotherChild.toString());

        System.out.println(adult.toString());
        System.out.println(anotherAdult.toString());



    }
}