package com.209032421.Proxy;

public class Proxy implements Message {

    private TrueMessage realMessage;

    private String message;

    public Proxy(String msg){
        this.message = msg;
    }
    public void showMessage() {

        if (realMessage == null) {
            //Create & Show Message only when it is required.
            realMessage = new TrueMessage(message);
        }
        realMessage.showMessage();
    }

    public String toString(){
        return message;
    }
}