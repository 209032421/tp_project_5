package com.209032421.Proxy;

public class RunProxy {

    public static void main(String[] args){
        Message message = new MessageProxy("Secret");
        System.out.println("Message before Proxy: \n"+ message+ "\n\n\nPost Proxy Message: \t");
        message.showMessage();
    }
}