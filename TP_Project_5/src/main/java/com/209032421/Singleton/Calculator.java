package com.209032421.Singleton;

public class Calculator{

    private static Calculator calculator = null;

    private Calculator(){
        /*This constructor is declared private to prevent
        external classes from creating an instance of this class.*/
    }
    public static Calculator getInstance(){
        if (calculator == null){
            calculator = new Calculator();
        }
        return calculator;
    }
    protected static int add(int a, int b){
        return (a+b);
    }
    protected static int subtract(int a, int b){
        return (a-b);
    }

    protected static int multiply(int a, int b){
        return (a*b);
    }

    protected static int quotient(int a, int b){
        return (a/b);
    }

}