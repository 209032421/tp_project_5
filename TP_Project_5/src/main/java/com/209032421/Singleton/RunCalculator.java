package com.209032421.Singleton;

public class RunCalculator{

    public static void main(String[] args){
        Calculator calculator = Calculator.getInstance();
        calculator.add(12, 24);
        calculator.subtract(18, 16);
        calculator.multiply(50, 10);
        calculator.quotient(50,2);

    }

}