package com.209032421.Strategy;


public class Context {
    int numEnemies;
    DetermineStrat strategy;

    public Context(int numEnemies, DetermineStrat strategy)
    {
        this.numEnemies = numEnemies;
        this.strategy  = strategy;
    }

    public void setStrategy(DetermineStrat strategy) {
        this.strategy = strategy;
    }

    public int getNumEnemies() {
        return numEnemies;
    }

    public void setNumEnemies(int numEnemies) {
        this.numEnemies = numEnemies;
    }

    public boolean getResult() {
        return strategy.checkArea(numEnemies);
    }