package com.209032421.Strategy;

public interface DetermineStrat{

    boolean checkArea(int enemies);

}