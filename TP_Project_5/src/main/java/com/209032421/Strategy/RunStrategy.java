package com.209032421.Strategy;

public class RunStrategy {

    public static void main(String[] args){
        int numEnemies = 60;

        DetermineStrat headsOn = new HeadsOnStrat();
        DetermineStrat ranged = new RangedStrat();

        Context context = new Context(numEnemies, headsOn);

        System.out.println("Number of Enemies: " + context.getNumEnemies());
        System.out.println("headsOn Attack possible: " + context.getResult());

        context.setStrategy(ranged);
        context.setNumEnemies(100);
        System.out.println("Number of Enemies: " + context.getNumEnemies());
        System.out.println("ranged possible: " + context.getResult());


    }
}