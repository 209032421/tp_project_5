package com.209032421.Bridge;

import org.junit.Assert;
import org.junit.Test;

public class TestBridge {

    @Test
    public void testBasic() throws Exception{
        Computer pcI7 = new BasicPC(new I7());
        pcI7.startPC();

        Assert.assertEquals( pcI7.getPowerRating(), 45, 0);
    }
    @Test
    public void testPerformance() throws Exception{
        Computer pcI7 = new PerformancePC(new I7());
        pcI7.startPC();

        Assert.assertEquals( pcI7.getPowerRating(), 70, 0);

    }
}