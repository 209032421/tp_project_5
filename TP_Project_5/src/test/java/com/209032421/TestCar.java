package com.209032421.Factory;

import org.junit.Assert;
import org.junit.Test;.209032421.Factory;

public class TestCar {
    @Test
    public void carDescription() throws Exception {
        BasicCar daewoo = new BasicCar();
        daewoo.carDescription();
    }

    @Test
    public void speed() throws Exception {
        BasicCar daewoo = new BasicCar();
        daewoo.speed();
        Assert.assertEquals(daewoo.speed(), 120);
    }

}