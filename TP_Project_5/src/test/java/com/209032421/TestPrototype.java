package com.209032421.Prototype;

import org.junit.Assert;
import org.junit.Test;

public class TestPrototype {
    @Test
    public void makeDuplicate() throws Exception {

        //Objects to clone
        Person adult = new Person ("Ellen", 24);

        //Clone objects
        Person newAdult = (Person) adult.makeDuplicate();
        /*Each product produces the same result. a copy of the first object but a different object in itself.
        Assert.assertEquals(adult, newAdult); Should fail!*/
        Assert.assertEquals(adult.toString(), newAdult.toString()); //The contents are the same.
    }

}