package com.209032421.Proxy;

import org.junit.Assert;
import org.junit.Test;

public class MessageProxyTest {
    @Test
    public void showMessage() throws Exception {
        Message message = new MessageProxy("Secret");
        message.showMessage();
        Assert.assertNotEquals("Secret", message.toString());
    }

}