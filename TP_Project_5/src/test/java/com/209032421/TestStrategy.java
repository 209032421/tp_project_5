package com.209032421.Strategy;

import org.junit.Assert;
import org.junit.Test;

public class TestStrategy {

    int numEnemies = 60;

    @Test
    public void testHeadsOnStrat() {
        Strategy heads = new HeadsOnStrat();
        Context context = new Context(numEnemies, heads);

        System.out.println("Number of Enemies: " + context.getNumEnemies());
        System.out.println("headson Attack possible: " + context.getResult());

        Assert.assertFalse(context.getResult());

    }

    @Test
    public void TestRangedStrat(){

        Strategy ranged = new RangedStrat();
        Context context = new Context(numEnemies, ranged);
        context.setNumEnemies(120);
        context.setStrategy((ranged));
        System.out.println("Number of Enemies: " + context.getNumEnemies());
        System.out.println("ranged Attack possible: " + context.getResult());
        Assert.assertEquals(120, context.getNumEnemies(), 0);
    }


}